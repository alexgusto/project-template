export default {
  entry: 'development/js/main.js',
  dest: 'production/js/main.min.js',
  format: 'iife',
  sourceMap: 'inline',
};