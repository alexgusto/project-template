var gulp = require("gulp")
  , imgMin = require("gulp-imagemin") 
  , postCss = require("gulp-postcss")
  , pug = require("gulp-pug")
  , sass = require("gulp-sass")
  , cssmin = require('gulp-minify-css')
  , watch = require("gulp-watch")
  , browserSync = require("browser-sync")
  , prefixer = require("gulp-autoprefixer");

var path = {
	    build: { //Тут мы укажем куда складывать готовые после сборки файлы
	        html: 'production/views',
	        js: 'production/js/',
	        css: 'production/css/',
	        img: 'production/img/',
	        fonts: 'production/fonts/'
	    },
	    src: { //Пути откуда брать исходники
	        html: 'development/*.html', //Синтаксис src/*.html говорит gulp что мы хотим взять все файлы с расширением .html
	        js: 'development/js/main.js',//В стилях и скриптах нам понадобятся только main файлы
	        style: 'development/sass/main.scss',
	        img: 'development/img/**/*.*', //Синтаксис img/**/*.* означает - взять все файлы всех расширений из папки и из вложенных каталогов
	        fonts: 'development/fonts/**/*.*'
	    },
	    watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
	        html: 'development/**/*.pug',
	        js: 'development/js/**/*.js',
	        style: 'development/sass/**/*.scss',
	        img: 'development/img/**/*.*',
	        fonts: 'development/fonts/**/*.*'
	    },
	    clean: './build'
};
